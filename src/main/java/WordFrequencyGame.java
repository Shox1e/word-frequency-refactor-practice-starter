import java.util.*;

public class WordFrequencyGame {

    public static final String Blank = "\\s+";

    public String getResult(String inputStr){

        try {
            //split the input string with 1 to n pieces of spaces
            List<Input> inputList = new ArrayList<>();
            addInputToList(inputStr,inputList);


            //get the map for the next step of sizing the same word
            inputList = countFrequency(inputList);

            StringJoiner result = getResult(inputList);
            return result.toString();
        } catch (Exception e) {


            return "Calculate Error";
        }

    }

    private static StringJoiner getResult(List<Input> inputList) {
        inputList.sort((preInput, curInput) -> curInput.getWordCount() - preInput.getWordCount());
        StringJoiner result = new StringJoiner("\n");
        inputList.forEach(input-> result.add(input.getValue() + " " +input.getWordCount()));
        return result;
    }

    private List<Input> countFrequency(List<Input> inputList) {
        Map<String, List<Input>> map = new HashMap<>();
        inputList.forEach(input ->map.computeIfAbsent(input.getValue(), k -> new ArrayList<>()).add(input));

        List<Input> list = new ArrayList<>();
        map.entrySet().forEach(m->list.add(new Input(m.getKey(),m.getValue().size())));
        return list;
    }

    private static void addInputToList(String inputStr,List<Input> inputList) {
        Arrays.asList(inputStr.split(Blank)).forEach(input->inputList.add(new Input(input,1)));
    }
}
